/*
* CPSC 3620 Spring 2020
* Copyright Everett Blakley 2020
* Everett Blakley <everett.blakley@uleth.ca>
*/

#include "Sliding_Solver.h"
#include <iostream>

using namespace std;

/**
 * Starting with Sliding_Solver.initial, use the A* Search Algorithm to find
 * the least number of moves to reach the goal configuration
 */
Solution Sliding_Solver::Solve_Puzzle(const string &goalConfig)
{
  count = 0;
  Board_Tile initial = Board_Tile(initalConfig, goalConfig);
  tileQueue.push(initial);

  /** Continue checking moves until we arrive at a solution, which will be the 
   * minimum number of moves possible, by the property of min heaps
   */
  while (tileQueue.top().getConfig() != goalConfig)
  {
    /* To avoid checking infinitely if board has no solution */
    if (count > max)
    {
      throw count;
    }

    /* Get the next possible moves */
    vector<Board_Tile> nextMoves = tileQueue.top().nextConfigs();

    for (vector<Board_Tile>::const_iterator move = nextMoves.begin(); move != nextMoves.end(); ++move)
    {
      if (!contains(*move))
      {
        /** This was supposed to cut down on the boards inserted into the 
         * tileQueue, but it's not working :( 
         */
        //pastBoards.insert(*move);
        tileQueue.push(*move);
      }
    }

    /* Remove the board with the largest heuristic */
    tileQueue.pop();
    count++;
  }

  /** Since we have exited the loop, we found a solution, so the moves of this
   * board will be the solution
   */
  Solution solution = Solution(tileQueue.top());

  /* Empty the queue for the next execution of the Solve_Puzzle method */
  tileQueue = priority_queue<Board_Tile, vector<Board_Tile>, Comparator>();
  return solution;
}

/* Utility method to check if configuration is already in the queue */
bool Sliding_Solver::contains(const Board_Tile &board) const
{
  return pastBoards.count(board) != 0;
}