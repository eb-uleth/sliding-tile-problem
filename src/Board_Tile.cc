/*
* CPSC 3620 Spring 2020
* Copyright Everett Blakley 2020
* Everett Blakley <everett.blakley@uleth.ca>
*/

#include "Board_Tile.h"
#include <iostream>

using namespace std;

Board_Tile::Board_Tile(const std::string &initialConfig, const string &goalConfig)
{
  config = initialConfig;
  goal = goalConfig;
  movesFromStart = "";
}

Board_Tile::Board_Tile(const Board_Tile &other)
{
  config = other.config;
  movesFromStart = other.movesFromStart;
  goal = other.goal;
}

Board_Tile &Board_Tile::operator=(const Board_Tile &other)
{
  config = other.config;
  movesFromStart = other.movesFromStart;
  goal = other.goal;
  return *this;
}

/** The heuristic for this problem is defined as
 * D(C) = A(C) + E(C), where:
 * A(C) is the number of moves the Board Tile has already undergone: numMoves
 * E(C) is the manhattan distance for the Board Tile: Manhattan_Distance()
 */
int Board_Tile::heuristic() const
{
  return this->numMoves() + this->Manhattan_Distance(goal);
}

bool Board_Tile::operator<(const Board_Tile &other) const
{
  return this->heuristic() < other.heuristic();
}

bool Board_Tile::operator>(const Board_Tile &other) const
{
  return this->heuristic() > other.heuristic();
}

/** Boolean to check if two boards a equal
 * We consider two boards equal if they have they same configuration
 * and moves from the start, and, if defined, the same goal
 */
bool Board_Tile::operator==(const Board_Tile &other) const
{
  return this->config == other.config;// && this->movesFromStart == other.movesFromStart && this->goal != "" ? this->goal == other.goal : true;
}

vector<Board_Tile> Board_Tile::nextConfigs() const
{
  bool debugMode = false;
  vector<Board_Tile> output;
  Position openPosition = Position(*this, '0');

  Board_Tile b = *this;
  string::const_reverse_iterator lastMove = movesFromStart.rbegin();

  if (debugMode)
  {
    printf("-------------\n");
    printf("Current board\n");
    this->printBoard();
    printf("Moves from start: %s\n", movesFromStart.c_str());
    printf("Last move: %c\n", *lastMove);
  }

  /* If the open space is not on the left side, return config.left */
  if (debugMode)
    printf("Moving left\n");
  if (openPosition.col != 0 && (movesFromStart == "" ||
                                *lastMove != 'R'))
  {
    Board_Tile newBoard = Board_Tile(*this);
    newBoard.config[openPosition.index] = newBoard.config[openPosition.index - 1];
    newBoard.config[openPosition.index - 1] = '0';
    newBoard.movesFromStart.push_back('L');
    output.push_back(newBoard);
  }
  else
  {
    if (debugMode)
      printf("Cannot move left\n");
  }

  /* If the open space is not on the right side, return config.right */
  if (debugMode)
    printf("Moving right\n");
  if (openPosition.col != N - 1 && (movesFromStart == "" ||
                                    *lastMove != 'L'))
  {
    Board_Tile newBoard = Board_Tile(*this);
    newBoard.config[openPosition.index] = newBoard.config[openPosition.index + 1];
    newBoard.config[openPosition.index + 1] = '0';
    newBoard.movesFromStart.push_back('R');
    output.push_back(newBoard);
  }
  else
  {
    if (debugMode)
      printf("Cannot move right\n");
  }

  /* If the open space is not on the bottom side, return config.bottom */
  if (debugMode)
    printf("Moving down\n");
  if (openPosition.row != N - 1 && (movesFromStart == "" ||
                                    *lastMove != 'U'))
  {
    Board_Tile newBoard = Board_Tile(*this);
    newBoard.config[openPosition.index] = newBoard.config[openPosition.index + N];
    newBoard.config[openPosition.index + N] = '0';
    newBoard.movesFromStart.push_back('D');
    output.push_back(newBoard);
  }
  else
  {
    if (debugMode)
      printf("Cannot move down\n");
  }

  /* If the open space is not on the top side, return config.up */
  if (debugMode)
    printf("Moving up\n");
  if (openPosition.row != 0 && (movesFromStart == "" ||
                                *lastMove != 'D'))
  {
    Board_Tile newBoard = Board_Tile(*this);
    newBoard.config[openPosition.index] = newBoard.config[openPosition.index - N];
    newBoard.config[openPosition.index - N] = '0';
    newBoard.movesFromStart.push_back('U');
    output.push_back(newBoard);
  }
  else
  {
    if (debugMode)
      printf("Cannot move up\n");
  }
  if (debugMode)
    printf("-------------\n");
  return output;
}

int Board_Tile::numMoves() const
{
  return movesFromStart.length();
}

int Board_Tile::Manhattan_Distance(const Board_Tile &goalconfig) const
{
  int sum = 0;
  for (int i = 0; i < N * N; i++)
  {
    sum += Manhattan_Distance(goalconfig, config[i]);
  }
  return sum;
}

int Board_Tile::Manhattan_Distance(const Board_Tile &goal, const char value) const
{
  if (value == '0')
    return 0;
  Position b1 = Position(*this, value);
  Position b2 = Position(goal, value);

  return abs(b1.row - b2.row) + abs(b1.col - b2.col);
}
