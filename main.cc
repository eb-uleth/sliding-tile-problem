/*
* CPSC 3620 Spring 2020
* Copyright Everett Blakley 2020
* Everett Blakley <everett.blakley@uleth.ca>
*/
#include <iostream>
#include <iomanip>
#include <set>

#include "Board_Tile.h"
#include "Sliding_Solver.h"

using namespace std;

void printBoard(const Board_Tile &b)
{
  cout << "config: " << b.getConfig() << ", ";
  cout << "num: " << b.numMoves() << ", ";
  cout << "h: " << b.heuristic() << ", ";
  cout << "moves: " << b.getMovesFromStart() << endl;
}

void compareBoards(const Board_Tile &b1, const Board_Tile &b2)
{
  string c1 = b1.getConfig();
  string c2 = b2.getConfig();
  printf("%c %c %c     %c %c %c \n", c1[0], c1[1], c1[2], c2[0], c2[1], c2[2]);
  printf("%c %c %c  -> %c %c %c \n", c1[3], c1[4], c1[5], c2[3], c2[4], c2[5]);
  printf("%c %c %c     %c %c %c \n", c1[6], c1[7], c1[8], c2[6], c2[7], c2[8]);
}

int main()
{
  string goalConfiguration = "123456780";
  vector<string> initialConfigurations = {
      "123745086",
      "436871052",
      "023156478",
      "236108457",
      "206138457",
      "236415780",
      "584310627",
  };

  cout << endl;
  cout << setw(12) << "Start Board";
  cout << setw(12) << "Goal Board";
  cout << setw(17) << "Number of moves";
  cout << setw(7) << "Moves" << endl;

  for (vector<string>::iterator c = initialConfigurations.begin();
       c != initialConfigurations.end(); ++c)
  {
    cout << setw(12) << *c;
    cout << setw(12) << goalConfiguration;
    Sliding_Solver solver = Sliding_Solver(*c);

    try
    {
      Solution solution = solver.Solve_Puzzle(goalConfiguration);

      cout << setw(17) << solution.numberOfMoves;
      cout << "  " << solution.moves << endl
           << endl;
    }
    catch (...)
    {
      cout << setw(17) << "No solution";
      cout << "  >" << solver.max << endl
           << endl;
    }
  }
  
  return 0;
}