/*
* CPSC 3620 Spring 2020
* Copyright Everett Blakley 2020
* Everett Blakley <everett.blakley@uleth.ca>
*/

#ifndef BOARD_TILE_H
#define BOARD_TILE_H

#include <string>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <assert.h>

class Position;
class Board_Tile
{
public:
  /* Constructor to create a Board_Tile from an initial configuration */
  Board_Tile(const std::string &initialConfig, const std::string &goalConfig = "");

  /* Copy Constructor */
  Board_Tile(const Board_Tile &other);

  /* Overloaded assignment operator */
  Board_Tile &operator=(const Board_Tile &other);

  /* Less than comparison operator */
  bool operator<(const Board_Tile &other) const;

  /* More than comparison operator */
  bool operator>(const Board_Tile &other) const;

  /* Boolean to check if two boards a equal */
  bool operator==(const Board_Tile &other) const;

  /* Destructor */
  ~Board_Tile(){};

  /* Returns at most 4 objects that are one move away from the current config */
  std::vector<Board_Tile> nextConfigs() const;

  /* Returns the number of moves the Board_Tile has taken from initial configuration */
  int numMoves() const;

  /* Returns the Manhattan Distance between the current configuration and the goal configuration */
  int Manhattan_Distance(const Board_Tile &goalconfig) const;

  /* Min Heap heuristic used to order Board Tiles inside a priority queue */
  int heuristic() const;

  /* Returns the current config */
  std::string getConfig() const
  {
    return config;
  }

  /* Returns a string of all the moves in order from the beginning */
  std::string getMovesFromStart() const
  {
    return movesFromStart;
  }

  /**
   * Prints the board to the console
   */
  void printBoard() const
  {
    printf("%c %c %c\n", config[0], config[1], config[2]);
    printf("%c %c %c\n", config[3], config[4], config[5]);
    printf("%c %c %c\n", config[6], config[7], config[8]);
    printf("\n");
  }

private:
  /* Current configuration of the Board_Tile */
  std::string config;

  /* Goal configuration for the Board Tile */
  std::string goal;

  /* A string of the moves (L, R, U, D) taken from initial configuration */
  std::string movesFromStart;

  /* Internal method to return the manhattan distance for an individual tile */
  int Manhattan_Distance(const Board_Tile &goal, char value) const;

  /* The number of rows and columns in the board */
  const int N = 3;

  /** return the row/column of the character in the configuration */
  int get_row(const char v) { return config.find(v) / N; }
  int get_col(const char v) { return config.find(v) % N; }

  /** 
   * Utility struct for comparing used to compare positions of strings characters
   * within the configuration string. Overkill, but oh well
   */
  struct Position
  {
    int index;
    int row;
    int col;
    Position(const Board_Tile &b, const char v)
    {
      index = b.config.find(v);
      row = index / b.N;
      col = index % b.N;
    }
    Position(const std::string config, const char v)
    {
      index = config.find(v);
      row = index / 3; // Assumes it'll always be 3x3 🤷
      col = index % 3;
    }
  };
};

#endif