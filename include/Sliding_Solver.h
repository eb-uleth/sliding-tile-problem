/*
* CPSC 3620 Spring 2020
* Copyright Everett Blakley 2020
* Everett Blakley <everett.blakley@uleth.ca>
*/

#ifndef SLIDING_SOLVER_H
#define SLIDING_SOLVER_H

#include <queue>
#include <set>
#include <assert.h>

#include "Board_Tile.h"

/* Utility struct for displaying the solution */
struct Solution
{
  int numberOfMoves;
  std::string moves;
  Solution(const Board_Tile &board) : numberOfMoves(board.numMoves()), moves(board.getMovesFromStart()) {};
};

/* Comparator class for inserting Board_Tiles into the min heap */
class Comparator
{
public:
  int operator()(const Board_Tile &b1, const Board_Tile &b2)
  {
    return b1 > b2;
  }
};

class Sliding_Solver
{
public:
  /* Constructs a Sliding_Solver given a string representation of the initial configuration */
  Sliding_Solver(const std::string &initial, const int &maxIterations = 5000)
      : max(maxIterations), initalConfig(initial){};

  /* Desctructor */
  ~Sliding_Solver(){};

  /* Solves the puzzle using A* search and returns the solution */
  Solution Solve_Puzzle(const std::string &goalConfig);

  /* Max number of iterations, to prevent infinitely checking */
  const int max;

private:
  /* Initial configuration */
  std::string initalConfig;

  /** Minimum Heap of Board Tiles, using Comparator class to ensure the min heap
   * property is satisfied
   */
  std::priority_queue<Board_Tile, std::vector<Board_Tile>, Comparator> tileQueue;

  int count = 0;

  /* Set of previous configurations (NOT IMPLEMENTED) */
  std::set<Board_Tile> pastBoards;
  
  /* Utility method to check if configuration is already in the queue */
  bool contains(const Board_Tile &board) const;
};

#endif