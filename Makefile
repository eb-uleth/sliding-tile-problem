CXX            := g++
CXX_LANG_FLAGS := -std=c++11 -g
CXX_WARN_FLAGS := -Wall -Wextra -pedantic

PROGRAM_DIR = build
PROGRAM = slidingTiles

OBJ_DIR := $(PROGRAM_DIR)
SRC     := \
	$(wildcard ./*.cc) \
	$(wildcard src/*.cc)
OBJECTS := $(SRC:%.cpp=$(OBJ_DIR)/%.o)


SRC_INCLUDE = include
INCLUDE = -I ${SRC_INCLUDE}

CXXFLAGS= $(CXX_LANG_FLAGS) $(CXX_WARN_FLAGS) $(INCLUDE)

all: build $(PROGRAM_DIR)/$(PROGRAM)
	$(PROGRAM_DIR)/$(PROGRAM)

$(OBJ_DIR)/%.o: %.cc
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@

$(PROGRAM_DIR)/$(PROGRAM): $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $(PROGRAM_DIR)/$(PROGRAM) $^

build:
	@mkdir -p $(PROGRAM_DIR)

.PHONY: all build clean
clean:
	rm -rvf $(PROGRAM_DIR)/*