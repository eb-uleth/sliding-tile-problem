# Sliding Tiles Program

**CPSC3620 Term Project by Everett Blakley (everettblakley@gmail.com)**

Implementation of an [A* Search algorithm](https://en.wikipedia.org/wiki/A*_search_algorithm) to solve a particular [Sliding Puzzle](https://en.wikipedia.org/wiki/Sliding_puzzle) known as the Sliding Tile problem. 

## Table of Contents

[TOC]

## Running the program

### Pre-programmed solutions

Running the program requires C++ 11, and use of the terminal. To run the pre-programmed tile solutions, run `make` from within the project directory, and the following output will be displayed:

```
 Start Board  Goal Board  Number of moves  Moves
   123745086   123456780                4  URRD

   436871052   123456780               18  URRULDDRULDLUURDRD

   023156478   123456780                4  DDRR

   236108457   123456780               10  DRUULLDDRR

   206138457   123456780               11  DDRUULLDDRR

   236415780   123456780               12  LLURRULLDDRR

   584310627   123456780               27  LURDLLDRRULLURRDLLDRULURDDR
```

Where Start Board refers to the initial [Board Tile](#Board_Tile Class) object, Goal Board is the goal, Number of moves is how many slides of the empty tile, and Moves represents the string of moves 

### Modifying the program

If you would like to change the inputs to the program, see `main.cc` for this [vector](http://www.cplusplus.com/reference/vector/vector/) of Board Tiles:

```C++
vector<string> initialConfigurations = {
      "123745086",
      "436871052",
      "023156478",
      "236108457",
      "206138457",
      "236415780",
      "584310627",
  };
```

Add whatever (valid) Board Tiles you would like to test the input.

## The Sliding Tile Problem

A The sliding tile problem is analogous to a physical [sliding puzzle game](https://en.wikipedia.org/wiki/Sliding_puzzle), and consists of solving a [`Board_Tile`](#Board_Tile), represented by a 3x3 matrix of integers from 0 to 8, where 0 is used as a place holder for an empty space. The current arrangement of values on the tiles is called the *configuration*. Tiles are slid up (U), down (D), left (L), or right (R) into the open space to attempt to rearrange them into the *goal configuration*. Below is an example of a board configuration:

![Board example](board.png)

In this configuration, 8 can be slid to the left and 7 can be slid down. This Board Tile can be represented by the string "123745086". Suppose the *goal configuration* "123456780", then the shortest solution to this puzzle is given by:

![Solition example](solution.png)

Further specification is given [below](#Board_Tile)

### Sliding_Solver: Putting the star in A*

The [`Sliding_Solver`](#Sliding_Solver) is what takes a `Board_Tile` and finds the solution to it. This is done using the [A* Search Algorithm](https://en.wikipedia.org/wiki/A*_search_algorithm). The TL;DR on A* is a graph traversal algorithm that uses a best-first approach to find a solution to the graph. This best-first approach attempts to find the path that minimizes the function `f(n) = g(n) + h(n)`, where `n` is a node, `g(n)` is the cost of the path from the beginning, and `h(n)$`is an estimate of the remaining cost to reach the goal. In our case, we are using the cumulative [Manhattan distance](https://en.wiktionary.org/wiki/Manhattan_distance) of all tiles to their goal position as the estimate. 

The `Sliding_Solver` maintains a [priority queue (or minimum heap)](http://www.cplusplus.com/reference/queue/priority_queue/) of `Board_Tiles`, with the top of the queue being the `Board_Tile` that contains the best solution so far. Once the top of the queue is equal to the goal, the program terminates (with some [exceptions](#Known issues)

`Sliding_Tile` objects are creating by specifying an initial `Board_Tile`, and (optionally) a maximum iteration count. The method `Sliding_Tile::Solve_Puzzle` executes the A* search as described above, provided with a goal configuration. 

## API

### Board_Tile

**Data Members**

| Member (`datatype name`)               | Description                                                  |
| -------------------------------------- | ------------------------------------------------------------ |
| `std::string config` (private)         | Current configuration of the Board                           |
| `std::string goal`(private)            | Configuration of the goal                                    |
| `std::string movesFromStart` (private) | Moves taken from the initial configuration. One of `U`, `D`, `R`, and `L` |

**Public Member Functions**

| Member function (`returnType name (parameters)`)             | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `Board_Tile(const std::string& initialConfig, const std::string goal)` | Constructor                                                  |
| `std::vector<Board_Tile> nextConfigs()`                      | Returns a vector of valid moves given the current configuration |
| `int numMoves`                                               | Returns the number of moves taken from the initial configuration |
| `int Manhattan_Distance(const Board_Tile& goalconfig)`       | Returns the cumulative Manhattan Distance of each tile to their location in the goal configuration |
| `int heuristic()`                                            | Returns the heuristic function, defined as the Number of moves from the initial configuration plus the cumulative Manhattan Distance to the goal |
| `std::string getConfig()`                                    | Returns the current configuration                            |
| `std::string movesFromStart()`                               | Returns the string representation of the moves taken from the initial configuration |
| `void printBoard()`                                          | Utilizes [printf](http://www.cplusplus.com/reference/cstdio/printf/) to print the current configuration of the board |

### Sliding_Solver

**Data Members**

| Member (`datatype name`)                                     | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `std::string initialConfig` (private)                        | Initial configuration to be solved                           |
| `std::priority_queue<Board_Tile, std::vector<Board_Tile>, Comparator> tileQueue` (private) | The priority queue (min heap) used to store moves taken by the Solver, utilizing the [Comparator](#Comparator) Utility Class |
| `int max` (public)                                           | Maximum number of iterations taken by `Solve_Puzzle`         |
| `int count`                                                  | Current number of iterations executed by `Solve_Puzzle`      |
| `std::set<Board_Tile> pastBoards` (private)                  | [Set](https://en.cppreference.com/w/cpp/container/set) of boards the solver has already considered (not implemented) |

**Public Member Functions**

| Member function (`returnType name (parameters)`)             | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `Sliding_Solver(const std::string &initial, const int& maxIterations)` | Constructor: sets the `initialConfig` and `max` data members |
| `Solution Solve_Puzzle(std::string& goalConfig)`             | Returns the [Solution](#Solution) to the puzzle, using the [A* search algorithm](#`Sliding_Solver`: Putting the * in A*) |
| `bool contains(Board_Tile& board)`                           | Determines if the board has been considered by the solver already |

### Utility Classes

#### Solution

A `Solution` is what is returned by `Sliding_Tile::Solve_Puzzle`, and consist of the number of moves, and the string representation of those moves to solve the puzzle.

```c++
struct Solution
{
  int numberOfMoves;
  std::string moves;
  Solution(const Board_Tile &board) : numberOfMoves(board.numMoves()), moves(board.getMovesFromStart()) {};
};
```

#### Comparator

The `Comparator` class is used by the `Sliding_Tile` class to prioritize `Board_Tiles` within the queue. It sorts based on the heuristic function described above.

```c++
class Comparator
{
public:
  int operator()(const Board_Tile &b1, const Board_Tile &b2)
  {
    return b1 > b2;
  }
};
```

#### Position

A `Position` is an internal class within `Board_Tile` used to compute the matrix position of a given character.

```c++
struct Position
{
  int index;
  int row;
  int col;
  Position(const Board_Tile &b, const char v)
  {
    index = b.config.find(v);
    row = index / b.N;
    col = index % b.N;
  }
};
```



## Known Issues

- Currently, the `Sliding_Solver` checks every board determined by the `Board_Tile::numMoves` method. Many of these boards may be identical to previously considered boards, and thus a computational waste of time and space. The intention was to implement a system using a `stl::set` to check if a `Board_Tile` had been considered. However, due to time constraints, and additional stress put on by COVID-19, this was not done. 

  In order to combat this, the constructor of the `Sliding_Solver` takes (optionally) a max iteration value, and will only compute solutions to puzzles with reasonable solutions. The default value is 5000. 

## License 

```
MIT License

Copyright (c) 2020 Everett Blakley

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

